<?php

namespace Tuc\ProductBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('TucProductBundle:Default:index.html.twig');
    }
}
